export interface OffensiveWordDTO {
  id?: string;
  word: string;
  level: number;
}
