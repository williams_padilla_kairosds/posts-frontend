export interface UserDTO {
  email: string;
  password: string;
  role?: String;
  nickName?: String;
}
