export interface OffensiveWordRequest {
  word: string;
  level: number;
}
