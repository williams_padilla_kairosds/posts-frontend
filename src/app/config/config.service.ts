import { Injectable } from '@angular/core';
import { Config } from './config';
import { ConfigProxyService } from './config-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  constructor(private proxy: ConfigProxyService) {}

  config: Config;
  load() {
    return new Promise<void>((resolve) => {
      this.proxy.getConfig().subscribe((config) => {
        this.config = config;
        console.log('config', this.config);
        resolve();
      });
    });
  }
}
