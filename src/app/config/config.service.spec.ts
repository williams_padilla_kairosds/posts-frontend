import { inject, TestBed } from '@angular/core/testing';
import { ConfigProxyFakeService } from './config-proxy-fake.service';
import { ConfigProxyService } from './config-proxy.service';
import { ConfigService } from './config.service';

describe('ConfigService', () => {
  let service: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ConfigService,
        {
          provide: ConfigProxyService,
          useClass: ConfigProxyFakeService,
        },
      ],
    });
    service = TestBed.inject(ConfigService);
  });

  it('should be created', inject([ConfigService], (service: ConfigService) => {
    expect(service).toBeTruthy();
  }));

  it('should load configuration', () => {
    const service: ConfigService = TestBed.inject(ConfigService);
    service.load();
    console.log('····>>>>', service.config.api);
    expect(service.config.api).not.toBeNull();
  });
});
