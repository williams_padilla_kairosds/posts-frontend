import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Config } from './config';

const CONFIG_FAKE: Config = {
  api: 'https://local-apps.kairosds.com/api',
};

@Injectable({
  providedIn: 'root',
})
export class ConfigProxyFakeService {
  constructor() {}

  getConfig(): Observable<Config> {
    return of(CONFIG_FAKE);
  }
}
