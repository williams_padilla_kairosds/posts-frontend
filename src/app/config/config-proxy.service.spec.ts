import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed, waitForAsync } from '@angular/core/testing';

import { ConfigProxyService } from './config-proxy.service';

describe('ConfigProxyService', () => {
  let service: ConfigProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ConfigProxyService],
    });
    service = TestBed.inject(ConfigProxyService);
  });

  it('should be created', inject(
    [ConfigProxyService],
    (service: ConfigProxyService) => {
      expect(service).toBeTruthy();
    }
  ));

  it('should get configuration', waitForAsync(() => {
    const service: ConfigProxyService = TestBed.inject(ConfigProxyService);
    service
      .getConfig()
      .subscribe((config) => expect(config.api).not.toBeNull());
  }));
});
