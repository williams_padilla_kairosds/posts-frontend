import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ConfigProxyService } from './config-proxy.service';
import { ConfigService } from './config.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [ConfigService, ConfigProxyService],
})
export class ConfigModule {}
