import { TestBed } from '@angular/core/testing';

import { ConfigProxyFakeService } from './config-proxy-fake.service';

describe('ConfigProxyFakeService', () => {
  let service: ConfigProxyFakeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigProxyFakeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
