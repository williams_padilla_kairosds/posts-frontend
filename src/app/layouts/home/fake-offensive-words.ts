import { OffensiveWord } from 'src/app/interfaces/offensive-word';
import { OffensiveWordDTO } from 'src/app/interfaces/offensive-word.dto';

export const FAKE_OFFENSIVE_WORDS: OffensiveWordDTO[] = [
  {
    id: '9396884b-aae6-45d2-b332-d025fddc2f81',
    word: 'shit',
    level: 3,
  },
  {
    id: '9396884b-aae6-45d2-b332-d025fddc2f83',
    word: 'useless',
    level: 2,
  },
  {
    id: '9396884b-aae6-45d2-b332-d025fddc2f85',
    word: 'fuck',
    level: 4,
  },
  {
    id: '9396884b-aae6-45d2-b332-d025fddc2f86',
    word: 'potato',
    level: 5,
  },
];

export const FAKE_ID: object = { id: '9396884b-aae6-45d2-b332-d025fddc2f86' };

export const FAKE_OFFENSIVE_WORDS_CREATED: OffensiveWord[] = [
  {
    _id: '9396884b-aae6-45d2-b332-d025fddc2f84',
    palabrota: 'shit',
    nivel: 3,
  },
  {
    _id: '9396884b-aae6-45d2-b332-d025fddc2f83',
    palabrota: 'useless',
    nivel: 2,
  },
  {
    _id: '9396884b-aae6-45d2-b332-d025fddc2f85',
    palabrota: 'fuck',
    nivel: 4,
  },
  {
    _id: '9396884b-aae6-45d2-b332-d025fddc2f86',
    palabrota: 'potato',
    nivel: 5,
  },
  {
    _id: '9396884b-aae6-45d2-b332-d025fddc4f86',
    palabrota: 'smash',
    nivel: 2,
  },
];

export const FAKE_UPDATED_OFFENSIVE_WORD: OffensiveWordDTO = {
  id: '9396884b-aae6-45d2-b332-d025fddc2f81',
  word: 'bullshit',
  level: 3,
};

export const FAKE_CREATED_OFFENSIVE_WORD: OffensiveWordDTO = {
  id: '9396884b-aae6-45d2-b332-d025fddc2f81',
  word: 'smash',
  level: 2,
};
