import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { OffensiveWord } from 'src/app/interfaces/offensive-word';
import { OffensiveWordStoreService } from './../../services/ofensive-word-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  offensiveWords$: Observable<OffensiveWord[]>;
  typeOfRequest: string;
  dataSent: OffensiveWord;
  form: FormGroup;

  constructor(private service: OffensiveWordStoreService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      word: new FormControl(''),
      level: new FormControl(''),
      id: new FormControl(''),
    });
    this.service.init();
    this.offensiveWords$ = this.service.get$();
    this.typeOfRequest = 'create';
  }

  async deleteOffensiveWord(id: string) {
    await this.service.deleteOffensiveWord(id);
  }

  toUpdate(data: OffensiveWord) {
    this.dataSent = data;
    this.typeOfRequest = 'update';
    this.form.get('word').setValue(data.palabrota);
    this.form.get('level').setValue(data.nivel);
    this.form.get('id').setValue(data._id);
  }

  update() {
    const data: OffensiveWord = {
      _id: this.dataSent._id,
      palabrota: this.form.get('word').value,
      nivel: this.form.get('level').value,
    };
    this.service.updateOffensiveWord(data);
    this.form.reset();
    this.typeOfRequest = 'create';
  }

  create() {
    const data: OffensiveWord = {
      palabrota: this.form.get('word').value,
      nivel: this.form.get('level').value,
    };
    this.service.createOffensiveWord(data);
    this.form.reset();
  }

  cancel() {
    this.form.reset();
    this.typeOfRequest = 'create';
  }
}
