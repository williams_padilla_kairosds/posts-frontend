import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { OffensiveWord } from '../../interfaces/offensive-word';
import { OffensiveWordStoreService } from '../../services/ofensive-word-store.service';
import { ProxyService } from '../../services/proxy.service';
import { ComponentsModule } from './../../components/components.module';
import {
  FAKE_CREATED_OFFENSIVE_WORD,
  FAKE_ID,
  FAKE_OFFENSIVE_WORDS,
  FAKE_UPDATED_OFFENSIVE_WORD,
} from './fake-offensive-words';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let storeService: OffensiveWordStoreService;
  let proxyService: ProxyService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        ComponentsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    storeService = TestBed.inject(OffensiveWordStoreService);
    proxyService = TestBed.inject(ProxyService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all offensive words', waitForAsync(() => {
    const spyOn = jest
      .spyOn(proxyService, 'getAll')
      .mockReturnValue(of(FAKE_OFFENSIVE_WORDS));

    component.ngOnInit();

    component.offensiveWords$.subscribe((offensiveWords: OffensiveWord[]) => {
      console.log('ofensiveWords: ', offensiveWords);
      expect(offensiveWords.length).toBe(4);
      expect(offensiveWords[0].nivel).toBe(3);
    });

    expect(spyOn).toHaveBeenCalled();

    spyOn.mockRestore();
  }));

  it('should create an offensive word', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxyService, 'getAll')
      .mockImplementation(() => of(FAKE_OFFENSIVE_WORDS));

    component.ngOnInit();

    const spyOnAdd = jest
      .spyOn(proxyService, 'createOffensiveWord')
      .mockReturnValue(of(FAKE_CREATED_OFFENSIVE_WORD));

    component.create();

    component.offensiveWords$.subscribe((offensiveWords: OffensiveWord[]) => {
      console.log(offensiveWords);
      expect(offensiveWords).toHaveLength(5);
    });

    expect(spyOnGet).toHaveBeenCalled();
    expect(spyOnAdd).toHaveBeenCalled();

    spyOnGet.mockRestore();
    spyOnAdd.mockRestore();
  }));

  it('should delete an offensive word by its id', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxyService, 'getAll')
      .mockImplementation(() => of(FAKE_OFFENSIVE_WORDS));

    component.ngOnInit();

    const id = '9396884b-aae6-45d2-b332-d025fddc2f86';

    const spyOnDelete = jest
      .spyOn(proxyService, 'deleteOffensiveWord')
      .mockImplementation(() => of(FAKE_ID));

    component.deleteOffensiveWord(id);

    component.offensiveWords$.subscribe((offensiveWords: OffensiveWord[]) => {
      console.log(offensiveWords);
      expect(offensiveWords).toHaveLength(3);
    });

    expect(spyOnGet).toHaveBeenCalled();
    expect(spyOnDelete).toHaveBeenCalled();

    spyOnGet.mockRestore();
    spyOnDelete.mockRestore();
  }));

  it('should update an offensive word', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxyService, 'getAll')
      .mockImplementation(() => of(FAKE_OFFENSIVE_WORDS));

    component.ngOnInit();

    const spyOnUpdate = jest
      .spyOn(proxyService, 'updateOffensiveWord')
      .mockImplementation(() => of(FAKE_UPDATED_OFFENSIVE_WORD));

    const data: OffensiveWord = {
      _id: '9396884b-aae6-45d2-b332-d025fddc2f81',
      palabrota: 'shit',
      nivel: 3,
    };
    component.toUpdate(data);
    component.update();

    component.offensiveWords$.subscribe((offensiveWords: OffensiveWord[]) => {
      console.log(offensiveWords);
      expect(offensiveWords).toHaveLength(4);
      expect(offensiveWords[0].palabrota).toBe(
        FAKE_UPDATED_OFFENSIVE_WORD.word
      );
    });

    expect(spyOnGet).toHaveBeenCalled();
    expect(spyOnUpdate).toHaveBeenCalled();

    spyOnGet.mockRestore();
    spyOnUpdate.mockRestore();
  }));
});
