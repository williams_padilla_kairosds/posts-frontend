import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserDTO } from 'src/app/interfaces/user.dto';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  created: boolean;
  userType: Array<string>;
  signUp$: Observable<void>;

  constructor(private service: AuthService) {}

  ngOnInit(): void {
    this.userType = ['AUTHOR', 'USER'];
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      role: new FormControl('', [Validators.required]),
      nickName: new FormControl('', [Validators.required]),
    });
    this.created = false;
  }

  onSubmit() {
    const userData: UserDTO = {
      email: this.form.get('email').value,
      password: this.form.get('password').value,
      role: this.form.get('role').value,
      nickName: this.form.get('nickName').value,
    };
    this.signUp$ = this.service.signUp(userData);
  }
}
