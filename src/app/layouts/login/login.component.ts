import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommonValidators } from 'src/app/comom-validators';
import { UserDTO } from 'src/app/interfaces/user.dto';
import { AuthStoreService } from 'src/app/services/auth/auth-store.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup;
  subs: Subscription;

  constructor(
    private service: AuthService,
    private router: Router,
    private store: AuthStoreService
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        CommonValidators.startWithNumber,
      ]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    const userInfo: UserDTO = {
      email: this.form.get('email').value,
      password: this.form.get('password').value,
    };
    // this.subs = this.service.login(userInfo).subscribe((response) => {
    //   localStorage.setItem('token', response.token);
    //   this.router.navigate(['offensive-words']);
    // });
    // this.subs.unsubscribe();
    this.subs = this.service.login(userInfo);
  }

  home() {
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
    // this.subs.unsubscribe();
  }
}
