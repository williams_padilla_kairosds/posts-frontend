import { TestBed } from '@angular/core/testing';

import { OffensiveWordService } from './offensive-word.service';

describe('OffensiveWordService', () => {
  let service: OffensiveWordService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OffensiveWordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
