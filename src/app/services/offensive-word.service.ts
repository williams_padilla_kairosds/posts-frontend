import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { OffensiveWord } from '../interfaces/offensive-word';
import { OffensiveWordDTO } from '../interfaces/offensive-word.dto';
import { ProxyService } from './proxy.service';

@Injectable({
  providedIn: 'root',
})
export class OffensiveWordService {
  constructor(private repository: ProxyService) {}

  // transforms input data to desire output through .pipe(function) to our domain

  getAll(): Observable<OffensiveWord[]> {
    return this.repository.getAll().pipe(
      map((offensiveWords: OffensiveWordDTO[]) => {
        return offensiveWords.map((ow) => {
          return {
            _id: ow.id,
            palabrota: ow.word,
            nivel: ow.level,
          };
        });
      })
    );
  }

  createOffensiveWord(offensiveWord: OffensiveWord): Observable<OffensiveWord> {
    const offensiveWordDto: OffensiveWordDTO = {
      word: offensiveWord.palabrota,
      level: offensiveWord.nivel,
    };

    return this.repository
      .createOffensiveWord(offensiveWordDto)
      .pipe(map(this.dtoToModel));
  }

  deleteOffensiveWord(offensiveWordId: string) {
    return this.repository.deleteOffensiveWord(offensiveWordId);
  }

  updateOffensiveWord(offensiveWord: OffensiveWord): Observable<OffensiveWord> {
    const offensiveWordDto: OffensiveWordDTO = {
      id: offensiveWord._id,
      word: offensiveWord.palabrota,
      level: offensiveWord.nivel,
    };

    return this.repository
      .updateOffensiveWord(offensiveWordDto)
      .pipe(map(this.dtoToModel));
  }

  dtoToModel(offensiveWord: OffensiveWordDTO): OffensiveWord {
    return {
      _id: offensiveWord.id,
      palabrota: offensiveWord.word,
      nivel: offensiveWord.level,
    };
  }
}
