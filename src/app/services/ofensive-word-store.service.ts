import { Injectable } from '@angular/core';
import { lastValueFrom, tap } from 'rxjs';
import { OffensiveWord } from '../interfaces/offensive-word';
import { Store } from '../state/store';
import { OffensiveWordService } from './offensive-word.service';

@Injectable({
  providedIn: 'root',
})
export class OffensiveWordStoreService extends Store<OffensiveWord[]> {
  constructor(private offensiveWordService: OffensiveWordService) {
    super();
  }

  init() {
    lastValueFrom(
      this.offensiveWordService
        .getAll()
        .pipe(tap((offensiveWords) => this.store(offensiveWords)))
    );
  }

  createOffensiveWord(offensiveWord: OffensiveWord) {
    return lastValueFrom(
      this.offensiveWordService
        .createOffensiveWord(offensiveWord)
        .pipe(
          tap((createdOffensiveWord) =>
            this.store([...this.get(), createdOffensiveWord])
          )
        )
    );
  }

  updateOffensiveWord(offensiveWord: OffensiveWord) {
    return lastValueFrom(
      this.offensiveWordService.updateOffensiveWord(offensiveWord).pipe(
        tap((updatedOffensiveWord) =>
          this.store(
            this.get().map((storedOffensiveWord) => {
              console.log(
                'store id:',
                storedOffensiveWord._id,
                'update id:',
                updatedOffensiveWord._id
              );
              return storedOffensiveWord._id === updatedOffensiveWord._id
                ? updatedOffensiveWord
                : storedOffensiveWord;
            })
          )
        )
      )
    );
  }

  deleteOffensiveWord(offensiveWordId: string) {
    return lastValueFrom(
      this.offensiveWordService
        .deleteOffensiveWord(offensiveWordId)
        .pipe(
          tap(() =>
            this.store(
              this.get().filter(
                (storedOffensiveWord) =>
                  storedOffensiveWord._id !== offensiveWordId
              )
            )
          )
        )
    );
  }
}
