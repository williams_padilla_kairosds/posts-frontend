import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { OffensiveWordDTO } from '../interfaces/offensive-word.dto';

@Injectable({
  providedIn: 'root',
})
export class ProxyService {
  constructor(private httpService: HttpClient, private env: ConfigService) {}

  getAll(): Observable<OffensiveWordDTO[]> {
    return this.httpService.get<OffensiveWordDTO[]>(
      `${this.env.config.api}offensive-word/`
    );
  }

  deleteOffensiveWord(id: string): Observable<object> {
    console.log(id);
    return this.httpService.delete<OffensiveWordDTO>(
      `${this.env.config.api}offensive-word/${id}`
    );
  }

  createOffensiveWord(data: OffensiveWordDTO): Observable<OffensiveWordDTO> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.httpService.post<OffensiveWordDTO>(
      `${this.env.config.api}offensive-word/`,
      {
        word: data.word,
        level: data.level,
      },
      { headers }
    );
  }

  updateOffensiveWord(data: OffensiveWordDTO): Observable<OffensiveWordDTO> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.httpService.put<OffensiveWordDTO>(
      `${this.env.config.api}offensive-word/${data.id}`,
      {
        word: data.word,
        level: data.level,
      },
      { headers }
    );
  }
}
