import { TestBed } from '@angular/core/testing';

import { OffensiveWordStoreService } from './ofensive-word-store.service';

describe('OffensiveWordStoreService', () => {
  let service: OffensiveWordStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OffensiveWordStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
