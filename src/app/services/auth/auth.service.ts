import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { RoleDTO } from '../../interfaces/role.dto';
import { User } from '../../interfaces/user';
import { UserDTO } from '../../interfaces/user.dto';
import { AuthProxyService } from './auth-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private repository: AuthProxyService, private router: Router) {}

  login(user: UserDTO): Subscription {
    return this.repository.login(user).subscribe(async (data) => {
      localStorage.setItem('token', data.token);
      await this.router.navigate(['/offensive-words']);
    });
  }

  signUp(user: UserDTO) {
    return this.repository.signUp(user);
  }

  getRole(): Observable<RoleDTO> {
    return this.repository.getRole();
  }

  dtoToModel(user: UserDTO): User {
    return {
      username: user.email,
      userPassword: user.password,
    };
  }
}
