import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthStoreService } from './auth-store.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuardService implements CanActivate {
  constructor(private store: AuthStoreService, private router: Router) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    await this.store.init();

    const role = this.store.get().role;
    console.log(role);

    if (role == 'ADMIN') return true;
    else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
