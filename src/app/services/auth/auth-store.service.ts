import { Injectable } from '@angular/core';
import { lastValueFrom, tap } from 'rxjs';
import { RoleDTO } from 'src/app/interfaces/role.dto';
import { Store } from 'src/app/state/store';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthStoreService extends Store<RoleDTO> {
  constructor(private authService: AuthService) {
    super();
  }

  init() {
    return lastValueFrom(
      this.authService.getRole().pipe(
        tap((data) => {
          console.log('data dentro del store:', data);
          this.store(data);
        })
      )
    );
  }
}
