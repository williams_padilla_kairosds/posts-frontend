import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from 'src/app/config/config.service';
import { RoleDTO } from 'src/app/interfaces/role.dto';
import { TokenDTO } from 'src/app/interfaces/token.dto';
import { UserDTO } from 'src/app/interfaces/user.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthProxyService {
  constructor(private httpService: HttpClient, private env: ConfigService) {}
  // authApi: string = ;

  login(data: UserDTO): Observable<TokenDTO> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.httpService.post<TokenDTO>(
      this.env.config.api.concat('login'),
      data,
      {
        headers,
      }
    );
  }

  signUp(data: UserDTO): Observable<void> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.httpService.post<void>(
      this.env.config.api.concat('sign-up'),
      data,
      {
        headers,
      }
    );
  }

  getRole(): Observable<RoleDTO> {
    return this.httpService.get<RoleDTO>(
      this.env.config.api.concat('auth/role/me')
    );
  }
}
