import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { OffensiveWordDTO } from '../interfaces/offensive-word.dto';
import { ProxyService } from './proxy.service';

describe('ProxyService', () => {
  let proxy: ProxyService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    proxy = TestBed.inject(ProxyService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(proxy).toBeTruthy();
  });

  it('Should get all offensive words', () => {
    const dummyOffensiveWordsArray: OffensiveWordDTO[] = [
      {
        id: 'hi',
        level: 2,
        word: 'hello',
      },
      {
        id: 'aaaa',
        level: 3,
        word: 'cacota',
      },
    ];
    proxy.getAll().subscribe((offensiveWords) => {
      console.log(offensiveWords);
      expect(offensiveWords.length).toBe(2);
      expect(offensiveWords).toBe(dummyOffensiveWordsArray);
    });

    const req = httpMock.expectOne(proxy.api);
    expect(req.request.method).toBe('GET');
    req.flush(dummyOffensiveWordsArray);
  });
});
