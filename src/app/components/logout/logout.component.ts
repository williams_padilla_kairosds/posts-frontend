import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent implements OnInit, OnDestroy {
  isLogged: boolean;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.isLogged = true;
    console.log('logged', this.isLogged);
  }

  logout() {
    if (localStorage.getItem('Logged').length > 0) {
      localStorage.clear();
      this.isLogged = false;
      this.router.navigate(['/']);
    }
  }

  ngOnDestroy(): void {}
}
