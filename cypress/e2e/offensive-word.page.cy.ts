describe('Offensive word page', () => {
  it('user should be able to access the page once logged', () => {
    cy.visit('/login');
    cy.findByPlaceholderText('email').type('willy@hola.com');
    cy.get('[placeholder="password"]').type('1234');
    cy.get('button').click();
    cy.get('form.ng-untouched').should('exist');
  });

  it('should be able to create a new offensive word', () => {
    cy.get('[placeholder="   word"]').type('shit');
    cy.get('[placeholder="   level"]').type('3');
    cy.get('.create').click();
    cy.get(':last-child> .offensive-word__title').contains('shit');
    cy.get(':last-child> p.text').contains('Level: 3');
  });

  it('should delete when trashcan is clicked', () => {
    cy.get(
      ':nth-child(4) > .offensive-word__buttons__container > .delete'
    ).click();
    cy.get(':nth-child(4) > .offensive-word__title').should('not.exist');
  });

  it('cancel button should be disable by default', () => {
    cy.get('[type="button"]').should('be.disabled');
  });

  it('should update the last offensive word when edit button is clicked', () => {
    //select the last item, should be 'aaaa' with level 3
    cy.get(
      ':last-child > .offensive-word__buttons__container > .update'
    ).click();
    cy.get('.create').should('not.exist');
    cy.get('.form_buttons__container > .update').should('exist');
    cy.get('[type="button"]').should('not.be.disabled');

    //verify that inputs now have the selected item to be updated
    cy.get('#word_input').should('have.value', 'idiot');
    cy.get('#level_input').should('have.value', '3');
    cy.get('#word_input').clear().type('Bullshit');
    cy.get('#level_input').clear().type('5');
    cy.get('.form_buttons__container > .update').click();

    //verify that inputs being cleared after updating
    cy.get('#word_input').should('have.value', '');
    cy.get('#level_input').should('have.value', '');
    cy.get('.form_buttons__container > .update').should('not.exist');

    //verify that the new item is displayed correctly
    cy.wait(0);
    cy.get(':last-child > .offensive-word__title').contains('Bullshit');
    cy.get(':last-child > p.text').contains('Level: 5');

    //Change the 3rd by default values 'aaaa' with level 3, so tests can run many times.
    cy.get(
      ':last-child > .offensive-word__buttons__container > .update'
    ).click();
    cy.get('#word_input').clear().type('idiot');
    cy.get('#level_input').clear().type('3');
    cy.get('.form_buttons__container > .update').click();
  });
});
