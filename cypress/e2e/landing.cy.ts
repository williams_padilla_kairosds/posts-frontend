describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/');
  });

  it('should go to login page, and title login must exist', () => {
    cy.visit('/');
    cy.get('.login__anchor').click();
    cy.get('.login__form > h1').should('exist');
  });

  it('should go to login page, and title login must exist', () => {
    cy.visit('/');
    cy.get('.sign-up__anchor').click();
    cy.get('.sign-up__form > h1').should('exist');
  });
});
