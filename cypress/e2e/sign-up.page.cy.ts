describe('Sign up page', () => {
  it('should visit sign up page', () => {
    cy.visit('/sign-up');
  });

  it('button "create" should be disabled if inputs are empty', () => {
    cy.get('button').should('be.disabled');
  });

  it('should log in when user is already in the database and has role admin', () => {
    cy.get('[placeholder="email"]').type('willy4@hola.com');
    cy.get('[placeholder="password"]').type('1234');
    cy.get('button').click();
    cy.get('.success__message').should('exist');
  });
});
