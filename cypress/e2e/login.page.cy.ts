describe('Login page', () => {
  it('should visit the login page ', () => {
    cy.visit('/login');
  });

  it('button "go" should be disabled if inputs are empty', () => {
    cy.get('button').should('be.disabled');
  });

  it('should log in when user is already in the database and has role admin', () => {
    cy.get('[placeholder="email"]').type('willy@hola.com');
    cy.get('[placeholder="password"]').type('1234');
    cy.get('button').click();
    cy.get('form.ng-untouched').should('exist');
    cy.visit('/login');
  });

  it('should return to landing page', () => {
    cy.get('.go-home__button').click();
  });
});
